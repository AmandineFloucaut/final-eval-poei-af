import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { QuestionsRoutingModule } from './pages/questions/questions-routing.module';
import { QuestionsModule } from './pages/questions/questions.module';
import { UsersModule } from './pages/users/users.module';
import { UsersRoutingModule } from './pages/users/users-routing.module';
import { HeaderComponent } from './components/partials/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    QuestionsRoutingModule,
    QuestionsModule,
    UsersModule,
    UsersRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
