import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  public username: string = "";
  public email: string = "";
  public password: string = "";

  constructor(public usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  public onSubmit(){
    const newUser: User = {
      tid: "",
      username: this.username,
      email: this.email,
      password: this.password,
      questions: []
    }

    this.usersService.addNewUser(newUser);
    this.router.navigate(['/users/login']);

  }

}
