import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Answer } from 'src/app/models/answer';
import { LoginService } from 'src/app/services/login.service';
import { QuestionsService } from 'src/app/services/questions.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-answer-form',
  templateUrl: './answer-form.component.html',
  styleUrls: ['./answer-form.component.scss']
})
export class AnswerFormComponent implements OnInit {

  public content: string ="";

  constructor(private questionsService: QuestionsService, private router: Router, private activatedRoute: ActivatedRoute, private loginService: LoginService, private usersService: UsersService) { }

  ngOnInit(): void {
    const currentUserSession = this.loginService.getCurrentUser();
     //console.log(currentUserSession); //ok
     const username = currentUserSession.username;
     //console.log(username); // ok
     this.usersService.getUserConnected(username);
  }

  public onSubmit(){
    const currentQuestionTid = this.activatedRoute.snapshot.params['tid'];

    let newAnswer: Answer = {
      tid: "",
      content: "",
      createdAt: "",
      questionTid: "",
      username: "",
      useremail: "",
      userTid: ""
    }
    if(this.usersService.currentUser?.tid) {
      newAnswer = {
        tid: "",
        content: this.content,
        createdAt: "",
        questionTid: currentQuestionTid,
        username: "",
        useremail: "",
        userTid: this.usersService.currentUser.tid
      }
    }
   
    this.questionsService.addNewAnswer(newAnswer);


    this.router.navigate(['/questions', currentQuestionTid]);
  }
}
