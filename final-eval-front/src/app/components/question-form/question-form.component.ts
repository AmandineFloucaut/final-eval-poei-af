import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/question';
import { UsersService } from 'src/app/services/users.service';
import { QuestionsService } from 'src/app/services/questions.service';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})
export class QuestionFormComponent implements OnInit {

  public title: string = "";
  public content: string = "";

  constructor(public questionsService: QuestionsService, private loginService: LoginService, private usersService: UsersService, private router: Router ) { }

  ngOnInit(): void {
     const currentUserSession = this.loginService.getCurrentUser();
     //console.log(currentUserSession); //ok
     const username = currentUserSession.username;
     //console.log(username); // ok
     this.usersService.getUserConnected(username);
  }

  public onSubmit(){

    console.log(this.usersService.currentUser?.tid);

    let newQuestion: Question = {
      tid: "",
      title: "",
      content: "",
      createdAt: "",
      username: "",
      useremail: "",
      userTid: "",
      answers: []
    };

    if(this.usersService.currentUser?.tid) {
      newQuestion = {
        tid: "",
        title: this.title,
        content: this.content,
        createdAt: "",
        answers: [],
        username: "",
        useremail: "",
        userTid: this.usersService.currentUser.tid
      }
    }

    this.questionsService.addNewQuestion(newQuestion);
    this.router.navigate(['/questions']);
  }

}



