import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/question';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.scss']
})
export class QuestionCardComponent implements OnInit {

  @Input()
  public question?: Question;
  public userEmail?: string;

  constructor(private router: Router, private usersService: UsersService) { }

  ngOnInit(): void {
    //console.log(this.question);
    //console.log(this.usersService.currentUser);
    
  }

  public isDetailsQuestionPage(): Boolean {
    const urlDetailsQuestion = "/questions/" + this.question?.tid;
    //console.log(urlDetailsQuestion);
    
    const currentRoute = this.router.url;
    //console.log(currentRoute);


    if(urlDetailsQuestion == currentRoute){
      return true;
    }
    return false;
  }

}
