import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { LoginService } from '../services/login.service';

@Injectable({providedIn: "root"})
export class AxiosConfiguration {

  constructor(private loginService: LoginService, private router: Router){}

  public axiosAuthInterceptor(){
    axios.interceptors.request.use(
      (config: AxiosRequestConfig) => {
        console.log("interception requête");

        if(config.headers && config.method !== 'get'){
          config.headers['Authorization'] = this.loginService.getCurrentUserBasicAuthentification();
        }
        return config;
      },

      error => {
        return Promise.reject(error)
      }
    );
  }

  public axiosErrorInterceptor(){
    axios.interceptors.response.use((response: AxiosResponse) => {
      console.log("interception réponse");

      return response;
    }, error => {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      // Do something with response error
      if(error.status === 401){
        this.loginService.logout();
        this.router.navigate(['users/login']);
      }
      return Promise.reject(error);
    });
  }

}