import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Question } from 'src/app/models/question';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  public user: User = {
    tid: "", username: "", email: "", password: "", questions: []
  };

  @Input()
  public question?: Question;

  constructor(public usersService: UsersService, private activatedRoute: ActivatedRoute, public loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    const nameUser = this.activatedRoute.snapshot.params['name'];
    console.log(nameUser);
    this.usersService.getOneUser(nameUser);
    console.log(this.usersService.currentUser);
    console.log(this.usersService.currentUser?.questions);

  }

  public clickOnDeleteUser(){
    this.usersService.deleteCurrentUser();
    this.loginService.verifyIfUserExist()
                    .then(() => this.router.navigate(['/']))
                    .catch(() => this.loginService.logout())
    // this.router.navigate(['/questions']);
    // this.loginService.logout();
  }

}
