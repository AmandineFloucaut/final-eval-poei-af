import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserToLogin } from 'src/app/models/userToLogin';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  public username: string = "";
  public password: string = "";

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {

  }

  public onSubmit(){
    console.log("submit login");
    const currentUser: UserToLogin = {
      username: this.username,
      password: this.password
    }
    this.loginService.login(currentUser);
    this.loginService.verifyIfUserExist()
                    .then(() => this.router.navigate(['/']))
                    .catch(() => this.loginService.logout())
  }

}
