import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsComponent } from './questions/questions.component';
import { QuestionsRoutingModule } from './questions-routing.module';
import { QuestionDetailsPageComponent } from './question-details-page/question-details-page.component';
import { QuestionCardComponent } from 'src/app/components/question-card/question-card.component';
import { QuestionFormComponent } from '../../components/question-form/question-form.component';
import { FormsModule } from '@angular/forms';
import { AnswerFormComponent } from 'src/app/components/answer-form/answer-form.component';


@NgModule({
  declarations: [
    QuestionsComponent,
    QuestionDetailsPageComponent,
    QuestionCardComponent,
    QuestionFormComponent,
    AnswerFormComponent
  ],
  imports: [
    CommonModule,
    QuestionsRoutingModule,
    FormsModule
  ]
})
export class QuestionsModule { }
