import { Component, OnInit } from '@angular/core';
import { response } from 'express';
import { Question } from 'src/app/models/question';
import { LoginService } from 'src/app/services/login.service';
import { QuestionsService } from 'src/app/services/questions.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  public question?: Question;

  constructor(public questionsService: QuestionsService, public loginService: LoginService) { }

  ngOnInit(): void{
    //console.log(this.questionsService.getQuestions());

    this.questionsService.getQuestions();
  }

}
