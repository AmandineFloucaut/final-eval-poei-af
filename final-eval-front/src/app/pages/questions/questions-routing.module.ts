import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnswerFormComponent } from 'src/app/components/answer-form/answer-form.component';
import { QuestionFormComponent } from 'src/app/components/question-form/question-form.component';
import { QuestionDetailsPageComponent } from './question-details-page/question-details-page.component';
import { QuestionsComponent } from './questions/questions.component';

const routes: Routes = [
  { path: 'questions', component: QuestionsComponent },
  { path: 'questions/add', component: QuestionFormComponent},
  { path: 'questions/:tid', component: QuestionDetailsPageComponent },
  { path: 'questions/add-answer/:tid', component: AnswerFormComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule { }
