import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { throwIfEmpty } from 'rxjs';
import { Question } from 'src/app/models/question';
import { User } from 'src/app/models/user';
import { LoginService } from 'src/app/services/login.service';
import { QuestionsService } from 'src/app/services/questions.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-question-details-page',
  templateUrl: './question-details-page.component.html',
  styleUrls: ['./question-details-page.component.scss']
})
export class QuestionDetailsPageComponent implements OnInit {

  @Input()
  public question?: Question;
  public tid: string = "";
  public isAddAnswer: boolean = false;
  public user?: User;

  constructor(public questionsService: QuestionsService, private activatedRoute: ActivatedRoute, public loginService: LoginService, public usersService: UsersService) { }

  ngOnInit(){
    this.tid = this.activatedRoute.snapshot.params['tid'];
    this.questionsService.getQuestionById(this.tid);
  }

  public addAnswer(): void {
    this.isAddAnswer = true;
  }

}
