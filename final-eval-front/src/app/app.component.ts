import { Component } from '@angular/core';
import { AxiosConfiguration } from './config/AxiosConfiguration';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'final-eval-front';

  constructor(private axiosConfiguration: AxiosConfiguration){
    axiosConfiguration.axiosAuthInterceptor();
    axiosConfiguration.axiosErrorInterceptor();
  }
}
