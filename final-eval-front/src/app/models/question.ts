import { Answer } from "./answer";

export interface Question {
    tid: string;
    title: string;
    content: string;
    createdAt: string;
    username: string;
    useremail: string;
    userTid: string;
    answers: Answer[];
}
