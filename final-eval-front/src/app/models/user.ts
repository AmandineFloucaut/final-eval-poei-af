export interface User {
    tid: string
    username: string;
    email: string;
    password: string;
    questions: [];
}
