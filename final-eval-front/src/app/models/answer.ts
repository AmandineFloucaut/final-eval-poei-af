export interface Answer {
    tid: string;
    content: string;
    createdAt: string;
    questionTid: string;
    username: string;
    useremail: string;
    userTid: string;
}
