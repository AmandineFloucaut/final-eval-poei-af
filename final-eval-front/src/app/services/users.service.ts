import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/user';
import { UserToLogin } from '../models/userToLogin';
import { LoginService } from './login.service';
import { QuestionsService } from './questions.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private apiUrl = "http://localhost:8080/api/users";
  public currentUser?: User;
  public questions?: [];

  constructor(private questionsService: QuestionsService) { }

  public addNewUser(user: User){
    axios.post(this.apiUrl, user);
  }

  public getUsersList(){
    axios.get(this.apiUrl);
  }

  public async getUserConnected(name: string){
    let currentUser = {
      username: name,
      email: name
    }
    await axios.post(this.apiUrl + "/one", currentUser)
    .then(response => {
      this.currentUser = response.data;
      console.log(this.currentUser);
    })
  }

  public getOneUser(nameOrEmail: string){
    const userToGet = {
      username: nameOrEmail,
      email: nameOrEmail
    }
    axios.post(this.apiUrl + "/one", userToGet)
    .then(response => {
      this.currentUser = response.data;
      console.log(this.currentUser);
      console.log(this.currentUser?.questions);
      this.questions = this.currentUser?.questions;
    })
  }

  public deleteCurrentUser(){
    if(this.currentUser){
      axios.delete(this.apiUrl + "/" + this.currentUser.tid)
            .then(() =>{
              this.questionsService.getQuestions();
              });
    }

  }
}
