import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../models/answer';
import { Question } from '../models/question';
import { LoginService } from './login.service';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  private apiUrl = "http://localhost:8080/api/questions";
  private apiUrlAnswer = "http://localhost:8080/api/answers"
  public questions: Question[] = [];
  public questionTid?: string;
  public question?: Question;

  constructor(private loginService: LoginService, private usersService: UsersService) { }

  /**
   * getQuestions -> get all questions in db
  */
  public getQuestions() {
    axios.get(this.apiUrl)
          .then(response => {
              this.questions = response.data;
              //console.log(this.questions);
          });
  }

  /**
   * getQuestionById
   */
  public getQuestionById(tid: string) {

    axios.get(this.apiUrl + '/' + tid)
          .then(response => {
            //console.log(response.data)
            this.question = response.data;
           //console.log(this.question);
           
          })
          .catch(error => console.log(error));

  }

  /**
   * addNewQuestion question: Question
   */
  public addNewQuestion(question: Question) {

    axios.post(this.apiUrl, question)
          .then(() => this.getQuestions());

  }

  /**
   * addNewAnswer
   */
  public addNewAnswer(answer: Answer) {
    console.log(answer);
    axios.post(this.apiUrlAnswer, answer)
        .then(() => {
          if(this.question?.tid)
          this.getQuestionById(this.question.tid)});

  }
}
