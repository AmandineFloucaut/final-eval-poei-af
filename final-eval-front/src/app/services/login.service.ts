import { Injectable } from '@angular/core';
import { User } from '../models/user';
import axios from 'axios';
import { UserToLogin } from '../models/userToLogin';
import { UsersService } from './users.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly SESSION_STORAGE_KEY = 'currentUser';
  private apiUrl: string = "http://localhost:8080/api/users";

  public user?: User;

  constructor(private usersService: UsersService) { }

  verifyIfUserExist(): Promise<any>{
    //console.log(axios.post(this.apiUrl + '/me'));

    return axios.post(this.apiUrl + '/me');
  }

  login(user: UserToLogin): void {
    // DOC - https://developer.mozilla.org/fr/docs/Web/API/Window/sessionStorage

    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    //console.log(user);
    
  }

  logout(): void {
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
  }

  getCurrentUserBasicAuthentification(): string {

    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    //console.log(currentUserPlain); // OK

    if(currentUserPlain){
      const currentUser = JSON.parse(currentUserPlain);
      //console.log(currentUser); // OK

      //console.log(btoa(currentUser.username + ":" + currentUser.password));

      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {

      return "";
    }
  }

  public userIsConnect(): boolean {
    if(sessionStorage.getItem('currentUser')){
      return true;
    }
    return false;
  }

  public getCurrentUser(){
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY);
    //console.log(currentUserPlain);
    if(currentUserPlain){
      const currentUser = JSON.parse(currentUserPlain);
      //console.log(currentUser); // OK

      //console.log(btoa(currentUser.username + ":" + currentUser.password));

      return currentUser;
    } else {

      console.log("pas de user connecté");

    }
  }
}
