import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionsModule } from './pages/questions/questions.module';
import { UsersModule } from './pages/users/users.module';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'questions' },
  { path: 'questions',
    loadChildren: () => import('./pages/questions/questions.module').then(m => m.QuestionsModule)
  },
  //{ path: 'users', pathMatch: 'full', redirectTo: 'users/login' },
  { path: 'users',
    loadChildren: () => import('./pages/users/users.module').then(m => m.UsersModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    QuestionsModule,
    UsersModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
