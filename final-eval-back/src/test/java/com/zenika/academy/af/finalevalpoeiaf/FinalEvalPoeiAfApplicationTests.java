package com.zenika.academy.af.finalevalpoeiaf;

import com.zenika.academy.af.finalevalpoeiaf.application.answers.AnswerManager;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.*;
import com.zenika.academy.af.finalevalpoeiaf.application.questions.QuestionManager;
import com.zenika.academy.af.finalevalpoeiaf.application.users.UserManager;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.QuestionRepository;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.UserRepository;
import com.zenika.academy.af.finalevalpoeiaf.mock.MockUser;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;


@SpringBootTest
@ActiveProfiles("test")
class FinalEvalPoeiAfApplicationTests {

	@Autowired
	private QuestionManager questionManager;
	@Autowired
	private AnswerManager answerManager;
	@MockBean
	private QuestionRepository questionRepositoryMock;
	@MockBean
	private UserRepository userRepositoryMock;
	@Autowired
	private UserManager userManager;

	private MockUser mockUser = new MockUser();
	public void FinalEvalApplication(QuestionRepository questionRepository, UserRepository userRepositoryMock, UserManager userManager){
		this.questionRepositoryMock = Mockito.mock(QuestionRepository.class);
		this.userRepositoryMock = Mockito.mock(UserRepository.class);
		this.userManager = userManager;
	}

	@Test
	void testApplication() throws UserExistException, UserNotFoundException, BadLengthTitleException,
								 BadLastCharTitleException {
		// Test user creation
		User newUser = this.userManager.createUser("testcreate@test", "testcreate", "testpasswordcreate");
		assertNotNull(newUser);

		//Test question creation
		/*Question newQuestion = this.questionManager.createQuestion("Question test ?", " Ceci est une question pour les test", newUser.getTid());
		assertNotNull(newQuestion);*/
	}

}
