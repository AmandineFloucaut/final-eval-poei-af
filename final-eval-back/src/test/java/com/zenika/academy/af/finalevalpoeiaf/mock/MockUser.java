package com.zenika.academy.af.finalevalpoeiaf.mock;

import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class MockUser {

    protected User mockUser;

    @Autowired
    public MockUser(){
        this.mockUser = new User("1b65858c-4292-475c-8bd1-eb85fa7e9dfe", "userTest", "userTest@test", "passwordTest");
    }

    public User getMockUser() {
        return mockUser;
    }

}
