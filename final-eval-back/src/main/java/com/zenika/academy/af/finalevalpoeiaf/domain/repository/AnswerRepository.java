package com.zenika.academy.af.finalevalpoeiaf.domain.repository;

import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnswerRepository extends CrudRepository<Answer, String> {

    List<Answer> findByQuestionTid(String questionTid);
}
