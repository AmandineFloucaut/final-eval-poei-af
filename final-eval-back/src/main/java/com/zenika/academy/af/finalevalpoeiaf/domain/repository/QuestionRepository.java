package com.zenika.academy.af.finalevalpoeiaf.domain.repository;

import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends CrudRepository<Question, String> {
    List<Question> findAllByOrderByCreatedAtDesc();
    // DOC - https://www.postgresql.org/docs/current/pgtrgm.html
    @Query(value = "SELECT * FROM questions WHERE SIMILARITY(title,?1) > 0.3", nativeQuery = true)
    List<Question> findByTitleLike(String title);

}
