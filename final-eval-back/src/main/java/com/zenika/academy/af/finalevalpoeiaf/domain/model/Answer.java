package com.zenika.academy.af.finalevalpoeiaf.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="answers")
@Access(AccessType.FIELD)
public class Answer {

    @Id
    private String tid;

    private String content;
    @CreatedDate
    @Column(name="created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_tid")
    private User user;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="question_tid")
    private Question question;

    protected Answer(){
        // For JPA
    }

    public Answer(String tid, String content, LocalDateTime createdAt, User user, Question question){
        this.tid = tid;
        this.content = content;
        this.createdAt = createdAt;
        this.question = question;
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTid() {
        return tid;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public Question getQuestion() {
        return question;
    }

    public User getUser() {
        return user;
    }


}
