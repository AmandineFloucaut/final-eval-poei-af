package com.zenika.academy.af.finalevalpoeiaf.web.users;

import com.zenika.academy.af.finalevalpoeiaf.web.questions.QuestionResponseDto;

import java.util.List;

public record UserResponseDto(String tid, String username, String email, List<QuestionResponseDto> questions) {
}
