package com.zenika.academy.af.finalevalpoeiaf.domain.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="questions")
public class Question {
    @Id
    private String tid;
    private  String title;
    private String content;
    @CreatedDate
    @Column(name="created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_tid")
    private User user;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST, mappedBy = "question")
    private List<Answer> answers;

    protected Question(){
        // For JPA
    }

    public Question(String tid, String title, String content, LocalDateTime createdAt, User user){
        this.tid = tid;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
        this.user = user;
        this.answers = new ArrayList<>();
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public User getUser() {
        return user;
    }

    public List<Answer> getAnswers() {
        return this.answers;
    }

}
