package com.zenika.academy.af.finalevalpoeiaf.web.answers;

import java.time.LocalDateTime;

public record AnswerResponseDto(String content, LocalDateTime createdAt, String username, String useremail,
                                String questionTid) {
}
