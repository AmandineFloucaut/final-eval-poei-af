package com.zenika.academy.af.finalevalpoeiaf.application.users;

import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserExistException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserManager {
    private final UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserManager(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public User createUser(String username, String email, String password) throws UserExistException {
        UserDetails currentUser = this.userRepository.findByUsernameOrEmail(username, email);
        if(currentUser != null){
            throw new UserExistException();
        }
        User newUser = new User(UUID.randomUUID().toString(),
                                username, email,
                                this.passwordEncoder.encode(password)
        );
        userRepository.save(newUser);
        return newUser;
    }

    public List<User> findAllUsers(){
        return (List<User>) this.userRepository.findAll();
    }

    public Optional<User> findByTid(String userTid){
        return userRepository.findById(userTid);
    }

    public Boolean userExist(String username , String email){
        if(this.userRepository.findByUsernameOrEmail(username, email) != null){
            return true;
        }
        return false;
    }

    public Optional<User> findUserByEmail(String email){
        Optional<User> user = this.userRepository.findByEmail(email);
        System.out.println(user);
        return user;
    }

    public UserDetails findUserByNameOrEmail(String username, String email){
        return this.userRepository.findByUsernameOrEmail(username, email);
    }


    public void deleteUser(User user){
        this.userRepository.delete(user);
    }
}
