package com.zenika.academy.af.finalevalpoeiaf.domain.repository;

import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    UserDetails findByUsernameOrEmail(String login, String login1);
    Optional<User> findByEmail(String email);
}
