package com.zenika.academy.af.finalevalpoeiaf.web.users;

public record CreateUserRequestDto(String username, String email, String password) {
}
