package com.zenika.academy.af.finalevalpoeiaf.web.users;

import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserExistException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.users.UserManager;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.web.answers.AnswerResponseDto;
import com.zenika.academy.af.finalevalpoeiaf.web.questions.QuestionResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/users")
public class UserController {

    private UserManager userManager;

    public UserController(UserManager userManager){
        this.userManager = userManager;
    }

    @PostMapping("/me")
    ResponseEntity<String> me(){
        System.out.println(SecurityContextHolder.getContext().getAuthentication().getName());
        return ResponseEntity.ok(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    @GetMapping()
    ResponseEntity<List<User>> getAllUsers(){
        return ResponseEntity.ok(this.userManager.findAllUsers());
    }

    @PostMapping()
    ResponseEntity<User> createUser(@RequestBody CreateUserRequestDto body) throws UserExistException {
        if(this.userManager.userExist(body.username(), body.email())){
            throw new UserExistException();
        }
        User newUser = this.userManager.createUser(body.username(), body.email(), body.password());
        return ResponseEntity.status(HttpStatus.CREATED).body(newUser);
    }
    @GetMapping("/{userTid}")
    ResponseEntity<UserResponseDto> getUser(@PathVariable String userTid) throws UserNotFoundException {
        if(this.userManager.findByTid(userTid).isEmpty()){
            throw new UserNotFoundException();
        }
        User currentUser = this.userManager.findByTid(userTid).orElseThrow();

        List<AnswerResponseDto> answersDto = new ArrayList<>();
        List<QuestionResponseDto> questionsDto = new ArrayList<>();

        for(Question question: currentUser.getQuestions()){
            for(Answer answer: question.getAnswers()){
                AnswerResponseDto answerResponseDto = new AnswerResponseDto(answer.getContent(),
                                                                            answer.getCreatedAt(),
                                                                            answer.getUser().getUsername(),
                                                                            answer.getUser().getEmail(),
                                                                            answer.getQuestion().getTid());
                answersDto.add(answerResponseDto);
            }
            QuestionResponseDto questionResponseDto = new QuestionResponseDto(question.getTid(),
                                                                                question.getTitle(),
                                                                                question.getContent(),
                                                                                question.getCreatedAt(),
                                                                                question.getUser().getUsername(),
                                                                                question.getUser().getEmail(),
                                                                                answersDto);
            questionsDto.add(questionResponseDto);
        }
        UserResponseDto userDto = new UserResponseDto(currentUser.getTid(), currentUser.getUsername(),
                                                      currentUser.getEmail(), questionsDto);
        return ResponseEntity.ok(userDto);
    }

    @PostMapping("/one")
    UserDetails getUserByNameOrEmail(@RequestBody FindUserByNameOrEmailRequestDto body) throws
                                                                                        UserNotFoundException {
        if(this.userManager.findUserByNameOrEmail(body.username(), body.email()) == null){
            throw new UserNotFoundException();
        }
        return this.userManager.findUserByNameOrEmail(body.username(), body.email());
    }


    @DeleteMapping("/{tid}")
    ResponseEntity<User> deleteUser(@PathVariable String tid) throws UserNotFoundException {
        User userToDelete = this.userManager.findByTid(tid).orElseThrow(UserNotFoundException::new);
        this.userManager.deleteUser(userToDelete);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
