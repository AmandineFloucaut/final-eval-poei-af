package com.zenika.academy.af.finalevalpoeiaf.web.exceptions;

import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = BadLengthTitleException.class)
    public final ResponseEntity<ApiError> badLengthTitleException(BadLengthTitleException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre ne peut pas dépasser 20 mots !"),
                                    HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadLastCharTitleException.class)
    public final ResponseEntity<ApiError> badLLastCharTitleException(BadLastCharTitleException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "Le titre doit se terminer par un '?' !"),
                                    HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = QuestionNotFoundException.class)
    public final ResponseEntity<ApiError> questionNotFoundException(QuestionNotFoundException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND, "La question n'a pas été trouvée !"),
                                    HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = BadLengthAnswerException.class)
    public final ResponseEntity<ApiError> badLengthAnswerException(BadLengthAnswerException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "La réponse ne peut pas dépasser 100 mots !"),
                                    HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserExistException.class)
    public final ResponseEntity<ApiError> userExistException(UserExistException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "L'utilisateur existe déjà !"),
                                    HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public final ResponseEntity<ApiError> userNotFoundException(UserNotFoundException exception){
        return new ResponseEntity<>(new ApiError(HttpStatus.BAD_REQUEST, "L'utilisateur n'a pas été trouvé !"),
                                    HttpStatus.BAD_REQUEST);
    }
}
