package com.zenika.academy.af.finalevalpoeiaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalEvalPoeiAfApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalEvalPoeiAfApplication.class, args);
	}

}
