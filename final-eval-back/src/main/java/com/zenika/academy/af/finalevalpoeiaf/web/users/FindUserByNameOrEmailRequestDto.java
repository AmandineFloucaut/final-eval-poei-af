package com.zenika.academy.af.finalevalpoeiaf.web.users;

public record FindUserByNameOrEmailRequestDto(String username, String email) {
}
