package com.zenika.academy.af.finalevalpoeiaf.domain.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name="users")
@Access(AccessType.FIELD)
public class User implements UserDetails {
    @Id
    private String tid;
    private String username;
    private String email;
    private String password;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST, mappedBy = "user")
    private List<Question> questions;

    @OneToMany(orphanRemoval = true, cascade = CascadeType.PERSIST, mappedBy = "user")
    private List<Answer> answers;

    protected User() {
        // For JPA
    }

    public User(String tid, String username, String email, String password){
        this.tid = tid;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getTid() {
        return tid;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Question> getQuestions() {
        return questions;
    }


}
