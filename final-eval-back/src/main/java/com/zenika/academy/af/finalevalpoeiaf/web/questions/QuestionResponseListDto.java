package com.zenika.academy.af.finalevalpoeiaf.web.questions;

import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.web.answers.AnswerResponseDto;

import java.time.LocalDateTime;
import java.util.List;

public record QuestionResponseListDto(String tid, String title, String content, LocalDateTime createdAt, String username,
                                      String useremail,
                                      List<Answer> answers) {
}
