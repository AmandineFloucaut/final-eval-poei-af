package com.zenika.academy.af.finalevalpoeiaf.web.answers;

import com.zenika.academy.af.finalevalpoeiaf.application.answers.AnswerManager;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLengthAnswerException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.QuestionNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.users.UserManager;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/answers")
public class AnswerController {
    private AnswerManager answerManager;
    private UserManager userManager;

    public AnswerController(AnswerManager answerManager, UserManager userManager){
        this.answerManager = answerManager;
        this.userManager = userManager;
    }

    @PostMapping()
    ResponseEntity<AnswerResponseDto> createAnswer(@RequestBody CreateAnswerDto body) throws
                                                                                         QuestionNotFoundException,
                                                                                         BadLengthAnswerException,
                                                                                         UserNotFoundException {
        Answer newAnswer = this.answerManager.createResponse(body.content(), body.userTid(), body.questionTid());

        User currentUser = this.userManager.findByTid(body.userTid()).orElseThrow(UserNotFoundException::new);
        AnswerResponseDto newAnswerDto = new AnswerResponseDto(body.content(), newAnswer.getCreatedAt(),
                                                               currentUser.getUsername(),
                                                               currentUser.getEmail(),
                                                               body.questionTid());

        return ResponseEntity.status(HttpStatus.CREATED).body(newAnswerDto);
    }
}
