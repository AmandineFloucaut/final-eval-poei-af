package com.zenika.academy.af.finalevalpoeiaf.web.questions;

import com.zenika.academy.af.finalevalpoeiaf.application.answers.AnswerManager;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLastCharTitleException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLengthTitleException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.QuestionNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.questions.QuestionManager;
import com.zenika.academy.af.finalevalpoeiaf.application.users.UserManager;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.web.answers.AnswerResponseDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/questions")
public class QuestionController {
    private final QuestionManager questionManager;
    private final AnswerManager answerManager;
    private final UserManager userManager;

    public QuestionController(QuestionManager questionManager, AnswerManager answerManager, UserManager userManager){
        this.questionManager = questionManager;
        this.answerManager = answerManager;
        this.userManager = userManager;
    }

    @GetMapping()
    ResponseEntity<List<QuestionResponseListDto>> getQuestionsList(){
        List<QuestionResponseListDto> questionsDto = new ArrayList<>();
        //List<AnswerResponseDto> answersDto = new ArrayList<>();

        for(Question question: this.questionManager.getQuestionsList()){
            /*System.out.println(question.getAnswers());
            if(question.getAnswers().size() > 1){
                for(Answer answer: question.getAnswers()){
                    AnswerResponseDto answerResponseDto = new AnswerResponseDto(answer.getContent(),
                                                                                answer.getCreatedAt(),
                                                                                answer.getUser().getUsername(),
                                                                                answer.getUser().getEmail(),
                                                                                answer.getQuestion().getTid());
                    answersDto.add(answerResponseDto);
                }
            }*/

            QuestionResponseListDto questionResponseDto = new QuestionResponseListDto(question.getTid(),
                                                                              question.getTitle(),
                                                                              question.getContent(),
                                                                              question.getCreatedAt(),
                                                                              question.getUser().getUsername(),
                                                                              question.getUser().getEmail(),
                                                                              question.getAnswers()
                                                                              );
            questionsDto.add(questionResponseDto);
        }

        return ResponseEntity.ok(questionsDto);
    }

    // TODO : manage exception QuestionExist
    @PostMapping()
    ResponseEntity<QuestionResponseDto> createQuestion(@RequestBody CreateQuestionDto body) throws BadLengthTitleException,
                                                                                                   BadLastCharTitleException,
                                                                                                   UserNotFoundException {
        if(this.userManager.findByTid(body.userTid()) == null){
            throw new UserNotFoundException();
        }
        Question newQuestion = this.questionManager.createQuestion(body.title(), body.content(), body.userTid());

        QuestionResponseDto newQuestionDto = new QuestionResponseDto(newQuestion.getTid(), body.title(),
                                                                     body.content(),
                                                                     newQuestion.getCreatedAt(),
                                                                     newQuestion.getUser().getUsername(),
                                                                     newQuestion.getUser().getEmail(),
                                                                     new ArrayList<>());
        return ResponseEntity.status(HttpStatus.CREATED).body(newQuestionDto);
    }

    @GetMapping("/{tid}")
    ResponseEntity<QuestionResponseDto> getQuestionById(@PathVariable String tid) throws QuestionNotFoundException {
        Question currentQuestion = this.questionManager.getQuestionById(tid).orElseThrow(QuestionNotFoundException::new);
        List<AnswerResponseDto> answersDto = new ArrayList<>();
        for(Answer answer: currentQuestion.getAnswers()){
            AnswerResponseDto answerResponseDto = new AnswerResponseDto(answer.getContent(),
                                                                        answer.getCreatedAt(),
                                                                        answer.getUser().getUsername(),
                                                                        answer.getUser().getEmail(),
                                                                        answer.getQuestion().getTid());
            answersDto.add(answerResponseDto);
        }
        QuestionResponseDto newQuestionDto = new QuestionResponseDto(currentQuestion.getTid(),
                                                                     currentQuestion.getTitle(),
                                                                     currentQuestion.getContent(),
                                                                     currentQuestion.getCreatedAt(),
                                                                     currentQuestion.getUser().getUsername(),
                                                                     currentQuestion.getUser().getEmail(),
                                                                     answersDto);
        return ResponseEntity.ok(newQuestionDto);
    }

    @PostMapping("/search")
    ResponseEntity<List<Question>> getPostsByTitleLike(@RequestBody String title){
        return ResponseEntity.ok(this.questionManager.getPostByTitleLike(title));
    }


    @GetMapping("/{tid}/answers")
    ResponseEntity<List<Answer>> getAnswersByQuestionTid(@PathVariable String tid){
        return ResponseEntity.ok(this.answerManager.getAnswersListByQuestion(tid));
    }


}
