package com.zenika.academy.af.finalevalpoeiaf.application.questions;

import com.zenika.academy.af.finalevalpoeiaf.application.answers.AnswerManager;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLastCharTitleException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLengthTitleException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.AnswerRepository;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.QuestionRepository;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class QuestionManager {
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    private final UserRepository userRepository;

    public QuestionManager(QuestionRepository questionRepository, AnswerRepository answerRepository, UserRepository userRepository){
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
        this.userRepository = userRepository;
    }
    public List<Question> getQuestionsList() {
        return this.questionRepository.findAllByOrderByCreatedAtDesc();
    }

    public Question createQuestion(String title, String content, String userTid) throws BadLengthTitleException,
                                                                                        BadLastCharTitleException,
                                                                                        UserNotFoundException {
        // Manage constraints title
        String[] wordsTitle = title.split(" ");
        // DOC - https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#substring(int)
        if(title.length() > 0){
            String lastChar = title.substring(title.length()-1);
            System.out.println(lastChar);
            if(wordsTitle.length > 20){
                throw new BadLengthTitleException();
            }
            if (!lastChar.equalsIgnoreCase("?")){
                throw new BadLastCharTitleException();
            }
        }

        User currentUser = this.userRepository.findById(userTid).orElseThrow(UserNotFoundException::new);

        // DOC - https://docs.oracle.com/javase/7/docs/api/java/lang/System.html#currentTimeMillis()
        Question newQuestion = new Question(UUID.randomUUID().toString(), title, content, LocalDateTime.now(), currentUser);
        this.questionRepository.save(newQuestion);
        return newQuestion;

    }

    public Optional<Question> getQuestionById(String tid) {
        return this.questionRepository.findById(tid);
    }

    public List<Answer> findAnswersQuestion(String questionTid){
        return this.answerRepository.findByQuestionTid(questionTid);
    }

    public List<Question> getPostByTitleLike(String title){
        return this.questionRepository.findByTitleLike(title);
    }

}
