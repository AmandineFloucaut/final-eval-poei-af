package com.zenika.academy.af.finalevalpoeiaf.configuration;

import com.zenika.academy.af.finalevalpoeiaf.domain.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class SecurityConfiguration {

    @Bean
    SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http
                .antMatcher("/**")
                .csrf().disable()
                .authorizeRequests(autorize -> autorize
                        .antMatchers(HttpMethod.GET, "/api/questions").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/questions/*").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/answers").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/answers/*").permitAll()
                        .antMatchers(HttpMethod.POST, "/api/users/one").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/users/one-user").permitAll()
                        .antMatchers(HttpMethod.POST, "/api/users").permitAll()
                        .anyRequest().authenticated()
                )
                .httpBasic()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().cors()
                .and().build();
    }

    /**
     * Pour autoriser tous les verbes http depuis le navigateur dans docker-compose
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("http://localhost/")
                        .allowedMethods("HEAD", "GET", "POST", "PUT", "DELETE", "PATCH");
            }
        };
    }

    @Bean
    UserDetailsService userDetailsService(UserRepository userRepository) {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
                return userRepository.findByUsernameOrEmail(login, login);
            }
        };
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

   /* @Bean
    public UserDetailsService users() {
        UserDetails amandineF = User.builder()
                                    .username("amandineF")
                                    .password("{bcrypt}$2y$10$cBJYJ.m/jJ6u9V2c4c/uJe3ojraYSlEtYapuf4iO7Oe8fW4D6.14W")
                                    .roles("ADMIN")
                                    .build();

        return new InMemoryUserDetailsManager(amandineF);
    }*/
}

