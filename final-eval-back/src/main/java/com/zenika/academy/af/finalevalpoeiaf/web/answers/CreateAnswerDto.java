package com.zenika.academy.af.finalevalpoeiaf.web.answers;

public record CreateAnswerDto(String content, String userTid, String questionTid) {
}
