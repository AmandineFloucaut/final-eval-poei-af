package com.zenika.academy.af.finalevalpoeiaf.web.questions;

public record CreateQuestionDto(String title, String content, String userTid) {
}
