package com.zenika.academy.af.finalevalpoeiaf.application.answers;

import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.BadLengthAnswerException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.QuestionNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.exceptions.UserNotFoundException;
import com.zenika.academy.af.finalevalpoeiaf.application.questions.QuestionManager;
import com.zenika.academy.af.finalevalpoeiaf.application.users.UserManager;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Answer;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.Question;
import com.zenika.academy.af.finalevalpoeiaf.domain.model.User;
import com.zenika.academy.af.finalevalpoeiaf.domain.repository.AnswerRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Component
public class AnswerManager {
    private AnswerRepository answerRepository;
    private QuestionManager questionManager;
    private UserManager userManager;

    public AnswerManager(AnswerRepository answerRepository, QuestionManager questionManager, UserManager userManager){
        this.answerRepository = answerRepository;
        this.questionManager = questionManager;
        this.userManager = userManager;
    }
    public Answer createResponse(String content, String userTid, String questionTid) throws QuestionNotFoundException,
                                                                                            BadLengthAnswerException,
                                                                                            UserNotFoundException {
        // Manage constraint number words content
        String[] wordsAnswer = content.split(" ");

        if (wordsAnswer.length > 100){
            throw new BadLengthAnswerException();
        }

        Question currentQuestion;
        if(questionTid != null){
            currentQuestion =
                    this.questionManager.getQuestionById(questionTid).orElseThrow();
        } else {
            throw new QuestionNotFoundException();
        }

        User currentUser;
        if(userTid != null){
            currentUser =
                    this.userManager.findByTid(userTid).orElseThrow(UserNotFoundException::new);
        } else {
            throw new UserNotFoundException();
        }

        Answer newAnswer = new Answer(UUID.randomUUID().toString(), content, LocalDateTime.now(), currentUser,
                                      currentQuestion);
        this.answerRepository.save(newAnswer);
        return newAnswer;
    }

    public List<Answer> getAnswersListByQuestion(String questionTid){
        return this.answerRepository.findByQuestionTid(questionTid);
    }
}
