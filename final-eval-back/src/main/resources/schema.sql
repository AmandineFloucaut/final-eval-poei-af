CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE TABLE if NOT EXISTS users (
  tid CHAR(36) NOT NULL PRIMARY KEY,
  username TEXT NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS questions (
    tid CHAR(36) NOT NULL PRIMARY KEY,
    title TEXT NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP,
    user_tid CHAR(36) REFERENCES users(tid)
);

CREATE TABLE IF NOT EXISTS answers (
    tid CHAR(36) NOT NULL PRIMARY KEY,
    content TEXT,
    created_at TIMESTAMP,
    question_tid CHAR(36) REFERENCES questions(tid),
    user_tid CHAR(36) REFERENCES users(tid)
);