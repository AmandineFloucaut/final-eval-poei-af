/* Insertion fake USERS */
INSERT INTO users(tid, username, email, password)
VALUES('e3e65cb2-c0ae-11ec-9d64-0242ac120002',
        'amandine',
        'amandine@gmail.com',
        '$2y$10$axuRyGnLXF7SKme7r9.AfOLk/Hp7BR8CUahWdLiAkbunAT6Rw.oz6')
ON CONFLICT (tid) DO NOTHING;

INSERT INTO users(tid, username, email, password)
VALUES('2b7127bf-f969-4540-ba41-eaada7843aa2',
        'albert',
        'albert@gmail.com',
        '$2y$10$MxdRP1kHHY7Kg2XH5V8JPuZ9r/KLOS4SzlR45VvMz9DA.ymXy01Xa')
ON CONFLICT (tid) DO NOTHING;

/* Insertion fake questions */
INSERT INTO questions(tid, title, content, created_at, user_tid)
VALUES('b403cc34-5929-4724-aeab-afc42537e737','Comment créer un projet Spring ?',
        'Je cherche un outils pour créer rapidement un projet Spring avec les dépendances nécessaires',
        '2022-05-01 11:40:00',
        'e3e65cb2-c0ae-11ec-9d64-0242ac120002')
ON CONFLICT (tid) DO NOTHING;

INSERT INTO questions(tid, title, content, created_at, user_tid)
VALUES('c0fe1934-0aed-45e3-9207-8c6b7e2db0f8',
    'Comment créer un projet angular ?',
    'Quelle est la commande pour créer un projet angular avec une structure basic ?',
    '2022-05-02 11:40:00',
    '2b7127bf-f969-4540-ba41-eaada7843aa2')
ON CONFLICT (tid) DO NOTHING;

/* Insertion fake answer */
INSERT INTO answers(tid, content, created_at, question_tid, user_tid)
VALUES('f718b7d3-c855-4b8b-b1c6-313258849003',
        'Vous pouvez utiliser SpringInitializr !',
         '2022-05-02 11:40:00',
         'b403cc34-5929-4724-aeab-afc42537e737',
         '2b7127bf-f969-4540-ba41-eaada7843aa2')
ON CONFLICT (tid) DO NOTHING;

INSERT INTO answers(tid, content, created_at, question_tid, user_tid)
VALUES('530666de-23ab-4dde-895f-024ed411fb46',
        'Effectivement SpringInitializr est très pratique, vous pouvez aussi utiliser le new project directement dans intellij et installer vos dépendances dans le pom.xml à la main !',
        '2022-05-02 11:40:00',
        'b403cc34-5929-4724-aeab-afc42537e737',
        '2b7127bf-f969-4540-ba41-eaada7843aa2')
ON CONFLICT (tid) DO NOTHING;

INSERT INTO answers(tid, content, created_at, question_tid, user_tid)
VALUES('1599c1af-6545-4773-8660-f88d2bb4f5f1',
        'Après après installer angular/cli, vous pouvez utiliser la commande ng new nameProject pour créer votre projet !',
        '2022-05-02 11:40:00',
        'c0fe1934-0aed-45e3-9207-8c6b7e2db0f8',
        'e3e65cb2-c0ae-11ec-9d64-0242ac120002')
ON CONFLICT (tid) DO NOTHING;